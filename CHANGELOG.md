## [1.1.1](https://gitlab.com/to-be-continuous/spectral/compare/1.1.0...1.1.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([4cb3a73](https://gitlab.com/to-be-continuous/spectral/commit/4cb3a73f03b6105a622f3571d9c3a44eed9ab00e))

# [1.1.0](https://gitlab.com/to-be-continuous/spectral/compare/1.0.0...1.1.0) (2024-1-27)


### Features

* migrate to CI/CD component ([e32d811](https://gitlab.com/to-be-continuous/spectral/commit/e32d811375854f5c6b12640bc635513f4316a924))

# 1.0.0 (2023-12-20)


### Features

* initial template ([3430391](https://gitlab.com/to-be-continuous/spectral/commit/343039136ba7439d5baabc03cb1e402068c81923))
