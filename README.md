# GitLab CI template for Spectral

This project implements a GitLab CI/CD template to lint JSON/YAML documents with
[Spectral](https://docs.stoplight.io/docs/spectral) with custom ruleset, and out of the box support for OpenAPI and AsyncAPI.

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/spectral/gitlab-ci-spectral@1.1.1
    # 2: set/override component inputs
    inputs:
      extra-args: "--ruleset custom.spectral.yaml" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/spectral'
    ref: '1.1.1'
    file: '/templates/gitlab-ci-spectral.yml'

variables:
  # 2: set/override template variables
  SPECTRAL_EXTRA_ARGS: "--ruleset custom.spectral.yaml" # ⚠ this is only an example
```

## Global configuration

The Spectral template uses some global configuration used throughout all jobs.

| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `image` / `SPECTRAL_IMAGE` | The Docker image used to run `spectral` | `registry.hub.docker.com/stoplight/spectral:latest` |

## Jobs

### `spectral` job

This job performs a [lint analysis](https://docs.stoplight.io/docs/spectral/docs/guides/2-cli.md) of your API document(s) (OpenAPI, AsyncAPI...), mapped to the `build` stage.

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `documents` / `SPECTRAL_DOCUMENTS` | Location of JSON/YAML documents to be linted. Can be either a file, a glob or fetchable resource(s) on the web          | `{,api/,src/main/resources/}*{openapi,oas,swagger,asyncapi}*.{json,yml,yaml}` <br><br>see [glob syntax](https://github.com/mrmlnc/fast-glob#basic-syntax) for more details |
| `extra-args` / `SPECTRAL_EXTRA_ARGS` | Extra arguments for the [Spectral CLI](https://docs.stoplight.io/docs/spectral/docs/guides/2-cli.md) | _none_ |
| `disabled` / `SPECTRAL_DISABLED` | Set to `true` to disable this job          | _none_ |


#### `$SPECTRAL_DOCUMENTS` default value

The default value is configured to lookup documents as below:

```
# Folder locations 
- ${project_root}
- ${project_root}/api
- ${project_root}/src/main/resources

# Filenames containing below keywords
- openapi
- oas
- swagger
- asyncapi

# File extentions
- json
- yaml
- yml
```
#### Ruleset File

If you don't specify an explicit ruleset file with the `--ruleset` option, the Spectral CLI looks for a ruleset file called `.spectral.yml`, `.spectral.yaml`, `.spectral.json` or `.spectral.js` in the current working directory.

If none is present in your project, the template creates a default `.spectral.yaml` file with below content:


```yaml
extends: spectral:oas           # out-of-the-box OpenAPI ruleset provided by Spectral
```

#### `$SPECTRAL_EXTRA_ARGS` use cases

The `$SPECTRAL_EXTRA_ARGS` can be used to override the defaults for optional arguments.
Below are the most probable use cases: 

- Custom Ruleset location

  Spectral uses the `.spectral.yaml` file in the root folder as convention for the ruleset.
  
  If there is a need to specify any other location (remote URL or other filename), the `$SPECTRAL_EXTRA_ARGS` variable shall be overridden.

- Fail-Severity

  Spectral default to `error` fail-severity.
  
  If there is a need to specify any other location (remote URL or other filename), the `$SPECTRAL_EXTRA_ARGS` variable shall be overridden.
